from tkinter import *
from tkinter import messagebox
import random
import copy
import time

#Sergio Akira Sarui Junior
#Python 3
#Biblioteca tkinter - conjunto de wrappers que implementam os widgets Tk como classes Python.

_estado_objetivo = '012345678'
_estado_inicial = '856204713'

# numeros vizinhos para cada quadrado
_vizinhos = {0: [1, 3],
              1: [0, 2, 4],
              2: [1, 5],
              3: [0, 4, 6],
              4: [1, 3, 5, 7],
              5: [2, 4, 8],
              6: [3, 7],
              7: [4, 6, 8],
              8: [5, 7]}

# Distâncias de Manhattan
_distancia = [[0, 1, 2, 1, 2, 3, 2, 3, 4],
             [1, 0, 1, 2, 1, 2, 3, 2, 3],
             [2, 1, 0, 3, 2, 1, 4, 3, 2],
             [1, 2, 3, 0, 1, 2, 1, 2, 3],
             [2, 1, 2, 1, 0, 1, 2, 1, 2],
             [3, 2, 1, 2, 1, 0, 3, 2, 1],
             [2, 3, 4, 1, 2, 3, 0, 1, 2],
             [3, 2, 3, 2, 1, 2, 1, 0, 1],
             [4, 3, 2, 3, 2, 1, 2, 1, 0]]

_opcoes = {1: "Busca Gulosa",
           2: "Busca A*"}

class QuebraCabecaOitoPecas(object):

    def __init__(self, input_state=None):
        if input_state:
            self.state = copy.deepcopy(input_state)
        else:
            # gera solucao randomica
            self.state = copy.deepcopy(_estado_objetivo)
            self.embaralhar()

    # embaralhar quebra cabeca
    def embaralhar(self):
        pos0 = self.state.index('0')
        for i in range(100):
            escolhas = _vizinhos[pos0]
            pos = escolhas[random.randint(0, len(escolhas) - 1)]
            self.trocar(pos)
            pos0 = self.state.index('0')

    # trocar a posicao 0 com o vizinho
    def trocar(self, pos):
        pos0 = self.state.index('0')
        l = list(self.state)
        l[pos0], l[pos] = l[pos], l[pos0]
        self.state = ''.join(l)

    # recuperar todos os próximos estados possíveis
    def pegar_proximo_estado(self, atual):
        pos0 = atual.index('0')
        proximosEstados = []

        for pos in _vizinhos[pos0]:
            l = list(atual)
            l[pos0], l[pos] = l[pos], l[pos0]
            passo = ''.join(l)
            proximosEstados.append(passo)
        return proximosEstados

    # Algoritmo de Busca Gulosa
    def busca_Gulosa(self):
        raiz = self.state
        objetivo = '012345678'
        anterior = {raiz: None}
        visitado = {raiz: True}
        solucao = (raiz == objetivo)
        q = [raiz]
        while q and not solucao:
            atual = q.pop(0)
            for proximo_no in self.pegar_proximo_estado(atual):
                if not proximo_no in visitado:
                    visitado[proximo_no] = True
                    anterior[proximo_no] = atual
                    q.append(proximo_no)
                if proximo_no == objetivo:
                    solucao = True
                    break

        # retorna o caminho mais curto
        if solucao:
            return self.caminho_r(objetivo, anterior), len(visitado)
        return None, len(visitado)

    # Algoritmo Busca A*
    def busca_A_Estrela(self, metodo):
        class No(object):
            def __init__(self, state):
                self.state = state
                self.g = 100000
                self.h = 100000

            def __str__(self):
                return self.state

            def f(self):
                return self.g + self.h

            def heurística(self, metodo):
                objetivo = '012345678'
                contar = 0
                if metodo == 1:
                    for i in range(9):
                        if self.state[0] != objetivo[0]:
                            contar += 1
                else:  # Distância de manhattan
                    for i in range(9):
                        pos = objetivo.index(self.state[i])
                        contar += _distancia[pos][i]
                self.h = contar

        raiz = No(self.state)
        raiz.g = 0
        raiz.heurística(metodo)
        objetivo = '012345678'
        anterior = {str(raiz): None}
        visitado = {str(raiz): True}
        solucao = (str(raiz) == objetivo)
        q = {raiz.f(): [raiz]}

        while not solucao and q:
            # sair da fila
            try:
                i = min(q)
            except Exception as e:
                continue
            try:
                atual = q[min(q)].pop()
            except Exception as e:
                del q[min(q)]
                continue

            visitado[str(atual)] = True
            for temp in self.pegar_proximo_estado(str(atual)):
                if temp in visitado:
                    continue
                no = No(temp)
                if no.g > atual.g + 1:
                    no.g = atual.g + 1
                    anterior[temp] = str(atual)
                no.heurística(metodo)
                if not no.f() in q:
                    q[no.f()] = []
                q[no.f()].append(no)

                if temp == objetivo:
                    solucao = True
                    break
        if solucao:
            return self.caminho_r(objetivo, anterior), len(visitado)
        return None, len(visitado)

    # retorno do caminho mais curto
    def caminho_r(self, objetivo, anterior):
        caminho = [objetivo]
        atual = objetivo
        while anterior[atual]:
            caminho.insert(0, anterior[atual])
            atual = anterior[atual]
        return caminho


quebra_cabeca = QuebraCabecaOitoPecas(_estado_inicial)


# tela exibir o estado atual do quebra-cabeça
def tela():
    cor = '#64737f' if quebra_cabeca.state != _estado_objetivo else '#38d12a'

    for i in range(9):
        if quebra_cabeca.state[i] != '0':
            variavel[i].set(str(quebra_cabeca.state[i]))
            label[i].config(bg=cor)
        else:
            variavel[i].set('')
            label[i].config(bg='white')


# resolver 8 quebra-cabeças usando um algoritmo específico
def resolver():
    for b in button:
        b.configure(state='disabled')
    opcao.configure(state='disabled')

    iniciar = {1: quebra_cabeca.busca_Gulosa,
               2: lambda: quebra_cabeca.busca_A_Estrela(2)}

    temp = select.get()
    index = 1
    for k, e in _opcoes.items():
        if e == temp:
            index = k
            break

    print('Resolvendo...')

    # recuperacao do tempo gasto para solucao
    stime = time.time()
    caminho, n = iniciar[index]()
    ttime = time.time()

    # Se o quebra cabeca nao for possivel resolver
    if not caminho:
        print('Esse quebra cabeça não e possível de resolver!')
        for i in range(9):
            label[i].config(bg='red' if quebra_cabeca.state[i] != '0' else 'white')
        for b in button:
            b.configure(state='normal')
        opcao.configure(state='normal')
        return

    info = 'Algoritmo: ' + _opcoes[index] + '\n' \
           + 'Tempo para encontrar solucao: ' + str(round(ttime - stime, 6)) + 's\n' \
           + 'Estados explorados: ' + str(n) + '\n' \
           + 'Solucao pelo caminho mais curto: ' + str(len(caminho) - 1) + ' passos.'
    print(info)
    operacao_da_tela(caminho)


def operacao_da_tela(caminho):
    if not caminho:
        for b in button:
            b.configure(state='normal')
        opcao.configure(state='normal')
        return
    quebra_cabeca.state = caminho.pop(0)
    tela()
    win.after(500, lambda: operacao_da_tela(caminho))


def embaralhar():
    quebra_cabeca.embaralhar()
    tela()

# redefinir para o estado inicial
def recomecar():
    quebra_cabeca.state = copy.deepcopy(_estado_inicial)
    tela()


# movimentacao das pecas pelo click
def movimentacao(event):
    text = event.widget.cget('text')
    if not text:
        return

    pos = quebra_cabeca.state.index(text)
    pos0 = quebra_cabeca.state.index('0')
    if _distancia[pos0][pos] > 1:
        return

    quebra_cabeca.trocar(pos)
    tela()


# Configuracoes do tk
win = Tk()
win.geometry('+300+100')
win.title('Quebra Cabeça de 8 Peças')
algoFrame = Frame(win, width=260, relief=RAISED)
algoFrame.pack()
select = StringVar(algoFrame)
select.set(_opcoes[1])  # default value
opcao = OptionMenu(algoFrame, select, _opcoes[1], _opcoes[2])
opcao.pack()
borda = Frame(win, width=260, height=260, relief=RAISED)
borda.pack()
variavel = [StringVar() for i in range(9)]
label = [Label(borda, textvariable=variavel[i], bg='gray', font=('Calibri', 48)) for i in range(9)]
for i in range(3):
    for j in range(3):
        label[i * 3 + j].bind("<Button-1>", lambda event: movimentacao(event))
        label[i * 3 + j].place(x=85 * j + 5, y=85 * i + 5, width=80, height=80)

buttonFrame = Frame(win, relief=RAISED, borderwidth=1)
buttonFrame.pack(fill=X, expand=True)
button = []
button.append(Button(buttonFrame, width='10', relief=RAISED, text="Recomecar", command=recomecar))
button.append(Button(buttonFrame, width='10', relief=RAISED, text="Embaralhar", command=embaralhar))
button.append(Button(buttonFrame, width='10', relief=RAISED, text="Resolver", command=resolver))  # to be initialized
for b in button:
    b.pack(side=LEFT, padx=5, pady=7)


# initialization of the game
def main():
    tela()
    win.mainloop()


if __name__ == "__main__":
    main()

